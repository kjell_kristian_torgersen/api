#ifndef __MCU_H__
#define __MCU_H__

#include "api/flash.h"
#include "api/ret.h"

void mcu_enableIRQs(void);
void mcu_disableIRQs(void);
void mcu_reset(void);
/** \brief Read from internal flash in MCU without any limitations. Used by the flash api and can be used typically for bootloaders.
 * \param addr address to read from
 * \param data pointer to data buffer
 * \param count number of bytes to read
 * \return RET_OK if successful
 */
ret_t mcu_flash_read(uint32_t addr, void* data, uint32_t count);
/** \brief Write to internal flash in MCU without any limitations. Used by the flash api and can be used typically for bootloaders.
 * \param addr address to write to
 * \param data pointer to data buffer
 * \param count number of bytes to write
 * \return RET_OK if successful
 */
ret_t mcu_flash_write(uint32_t addr, const void* data, uint32_t count);

ret_t mcu_flash_erase(uint32_t addr, uint32_t count);
const flash_get_size_t* mcu_flash_info(void);
#endif
