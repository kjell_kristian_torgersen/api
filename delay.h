#ifndef API_DELAY_H_
#define API_DELAY_H_

#include <stdint.h>

void delay_init(void);
void delay_us(uint32_t us);
void delay_ms(uint32_t ms);

#endif /* API_DELAY_H_ */
