#ifndef AF600FE9_449B_4B9A_9429_CFA0DF24E267
#define AF600FE9_449B_4B9A_9429_CFA0DF24E267

void allocate_init(void* storage, unsigned storage_size);
void* allocate(unsigned size);
unsigned allocate_usage(void);

#endif /* AF600FE9_449B_4B9A_9429_CFA0DF24E267 */
