#ifndef A88918D8_FA65_4D46_A257_FB12411C096E
#define A88918D8_FA65_4D46_A257_FB12411C096E

#include <stdint.h>

#include "api/ret.h"

typedef struct uart uart_t;

typedef void (*uart_tx_done_t)(void* arg);

typedef enum uart_stopbits { UART_SB_ONE,
    UART_SB_ONEANDHALF,
    UART_SB_TWO } uart_stopbits_t;

/** \brief configure the uart object
 *  \param this pointer to the uart object
 *  \param baudrate the baudrate of the uart
 *  \param stopbits the stopbits of the uart
 *  \param parity the parity of the uart, 'N'one, 'E'ven, 'O'dd
 *  \param databits the databits of the uart
 *  \return RET_OK if successful
 */
ret_t uart_configure(uart_t* this, uint32_t baud_rate, char parity, uart_stopbits_t stopbits, int databits);
ret_t uart_set_tx_done_callback(uart_t* this, uart_tx_done_t callback, void* arg);
ret_t uart_read(uart_t* this, void* buf, int count);
ret_t uart_write(uart_t* this, const void* buf, int count);
ret_t uart_close(uart_t* this);

#endif /* A88918D8_FA65_4D46_A257_FB12411C096E */
