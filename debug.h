#ifndef D92E68D8_DB05_4C44_B6C6_E42AC1FC721D
#define D92E68D8_DB05_4C44_B6C6_E42AC1FC721D

/** @brief printf-function to send data to the user-interface (if available). The intended use of this function is for debugging messages
 * @param format printf-format string
 */
int debug_printf(const char *restrict format, ...);
int debug_vprintf(const char *restrict format, va_list ap);

#endif /* D92E68D8_DB05_4C44_B6C6_E42AC1FC721D */
