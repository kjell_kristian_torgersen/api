/*
 * i2c.h
 *
 *  Created on: Oct 22, 2020
 *      Author: kjell
 */

#ifndef API_I2C_H_
#define API_I2C_H_

#include <stdbool.h>
#include <stdint.h>

#include "api/ret.h"

typedef struct i2c i2c_t;

ret_t i2c_device_is_ready(i2c_t* this, uint8_t address, uint8_t trials);
ret_t i2c_read(i2c_t* this, uint8_t address, void* buf, int count);
ret_t i2c_write(i2c_t* this, uint8_t address, const void* buf, int count);
ret_t i2c_scan(uint8_t addr);

#endif /* API_I2C_H_ */
