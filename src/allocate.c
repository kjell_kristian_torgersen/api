#include <stddef.h>
#include <stdint.h>

#include "../allocate.h"

static uint32_t* pool = NULL;
static unsigned idx = 0;
static unsigned capacity = 0;

void allocate_init(void* storage, unsigned storage_size)
{
    pool = storage;
    idx = 0;
    capacity = storage_size;
}

void* allocate(unsigned size)
{
    int aligned_size = (size + 3) & ~3;
    if (idx + aligned_size > capacity) {
        return NULL;
    }
    void* ret = &pool[idx];
    idx += aligned_size;
    return ret;
}

unsigned allocate_usage(void)
{
    return idx;
}