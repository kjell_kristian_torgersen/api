#include <stdint.h>

#include "api/allocate.h"
#include "api/flash.h"
#include "api/gpio.h"
#include "api/spi.h"
#include "utils/misc.h"

#define CHIP_ENABLE() gpio_clear_pinmask(this->cs_port, 1 << this->cs_pin)
#define CHIP_DISABLE() gpio_set_pinmask(this->cs_port, 1 << this->cs_pin)

struct flash {
    const flash_vtable_t* vtable;
    spi_t* spi;
    uint32_t start_addr;
    uint32_t size;
    uint8_t cs_port;
    uint8_t cs_pin;
};

flash_t* flash_spi_init(spi_t* spi, uint8_t cs_port, uint8_t cs_pin, uint32_t start_addr, uint32_t size)
{
    flash_t* this = allocate(sizeof(flash_t));
    if (this) {
        this->spi = spi;
        this->start_addr = start_addr;
        this->size = size;
        this->cs_port = cs_port;
        this->cs_pin = cs_pin;
        CHIP_DISABLE();
        gpio_config(this->cs_port, this->cs_pin, GPIO_OUTPUT);
    }
    return this;
}

static int spiflash_read_page(flash_t* this, uint16_t page, uint8_t offset, void* buf, int count)
{
    uint8_t cmd[4] = { 0x3, (page >> 8), (page & 0xFF), offset };
    CHIP_ENABLE();
    spi_write(this->spi, cmd, 4);
    spi_read(this->spi, buf, count);
    CHIP_DISABLE();
    return count;
}

static void command(flash_t* this, uint8_t cmd)
{
    CHIP_ENABLE();
    spi_write(this->spi, &cmd, 1);
    CHIP_DISABLE();
}

static int spiflash_write_page(flash_t* this, uint16_t page, uint8_t offset, const void* buf, int count)
{
    uint8_t cmd[4] = { 0x2, (page >> 8), (page & 0xFF), offset };
    command(this, 0x6);
    CHIP_ENABLE();
    spi_write(this->spi, cmd, 4);
    spi_write(this->spi, buf, count);
    CHIP_DISABLE();
    return count;
}

static int spiflash_erase(flash_t* this, uint16_t page, int count)
{
    command(this, 0x6);
    uint8_t cmd[4] = { 0x20, (page >> 8), (page & 0xFF), 0 };
    CHIP_ENABLE();
    spi_write(this->spi, cmd, 4);
    CHIP_DISABLE();
    return 4096;
}

static int spiflash_write(flash_t* this, uint32_t address, const void* buf, int count)
{
    uint8_t offset = address & 0xFF;
    uint32_t page = address & (~0xFF);

    // If it is required to write a partial page first, read required amount of bytes
    if (offset) {
        uint16_t bytesToWrite = MIN(256 - offset, count);
        spiflash_write_page(this, page, offset, buf, bytesToWrite);
        spiflash_write(this, 256 * (page + 1), (uint8_t*)buf + bytesToWrite, count - bytesToWrite);
    } else {
        int pages = count / 256;
        for (int i = 0; i < pages; i++) {
            spiflash_write_page(this, i, 0, (uint8_t*)buf + 256 * i, count);
        }
        uint32_t rest = count & 0xFF;
        if (rest) {
            spiflash_write_page(this, pages, 0, (uint8_t*)buf + 256 * pages, rest);
        }
    }
    return count;
}

static int spiflash_read(flash_t* this, uint32_t address, void* buf, int count)
{
    uint8_t offset = address & 0xFF;
    uint32_t page = address & (~0xFF);

    // If it is required to read a partial page first, read required amount of bytes
    if (offset) {
        uint16_t bytesToRead = MIN(256 - offset, count);
        spiflash_read_page(this, page, offset, buf, bytesToRead);
        spiflash_read(this, 256 * page, (uint8_t*)buf + bytesToRead, count - bytesToRead);
    } else {
        int pages = count / 256;
        for (int i = 0; i < pages; i++) {
            spiflash_read_page(this, i, 0, (uint8_t*)buf + 256 * i, count);
        }
        uint32_t rest = count & 0xFF;
        if (rest) {
            spiflash_read_page(this, pages, 0, (uint8_t*)buf + 256 * pages, rest);
        }
    }
    return count;
}
