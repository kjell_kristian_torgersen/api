#include <string.h>

#include "api/allocate.h"
#include "api/flash.h"
#include "api/mcu.h"

typedef struct flash {
    const flash_vtable_t* vtable;
    uint32_t start_addr;
    uint32_t size;
} flash_t;

static ret_t f_read(flash_t* this, uint32_t addr, void* data, unsigned count)
{
    if (this->start_addr + addr + count > this->start_addr + this->size) {
        return RET_ERROR;
    }
    return mcu_flash_read(this->start_addr + addr, data, count);
}

static ret_t f_write(flash_t* this, uint32_t addr, const void* data, unsigned count)
{
    if (this->start_addr + addr + count > this->start_addr + this->size) {
        return RET_ERROR;
    }
    return mcu_flash_write(this->start_addr + addr, data, count);
}

static ret_t f_erase(flash_t* this, uint32_t addr, uint32_t count)
{
    if (this->start_addr + addr + count > this->start_addr + this->size) {
        return RET_ERROR;
    }
    return mcu_flash_erase(this->start_addr + addr, count);
}

static ret_t f_command(flash_t* this, flash_commands_t command, void* arg)
{
    switch (command) {
    case FLASH_GET_SIZE: {
        *(flash_get_size_t*)arg = *mcu_flash_info();
    }

    break;
    case FLASH_GET_STATUS:
        *(uint32_t*)arg = 0;
        break;
    case FLASH_BUSY:
        *(bool*)arg = false;
        break;
    case FLASH_SET_BLOCKING:
        break;
    }
    return RET_OK;
}

static ret_t f_close(flash_t* this)
{
    return RET_OK;
}

static const flash_vtable_t vtable = {
    NULL,
    f_read,
    f_write,
    f_erase,
    f_command,
    f_close,
};

flash_t* flash_mcu_init(uint32_t start_addr, uint32_t size)
{
    flash_t* this = allocate(sizeof(flash_t));
    if (this) {
        this->vtable = &vtable;
        this->start_addr = start_addr;
        this->size = size;
    }
    return (struct flash*)this;
}