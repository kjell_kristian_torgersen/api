#include <stdarg.h>
#include <stddef.h>

#include "api/allocate.h"
#include "api/io.h"
#include "api/uart.h"

struct io {
    const io_vtable_t* vtable;
    uart_t* uart;
};

static int ioread(io_t* this, void* buf, int len)
{
    return uart_read(this->uart, buf, len);
}

static int iowrite(io_t* this, const void* buf, int len)
{
    return uart_write(this->uart, buf, len);
}

static int ioseek(io_t* this, int offset, io_whence_t whence)
{
    return -1;
}

static int ioctl(io_t* this, int request, va_list arg)
{
    return -1;
}

static void ioflush(io_t* this)
{
    uart_flush(this->uart);
}

static void ioclose(io_t* this)
{
    uart_close(this->uart);
    this->vtable = NULL;
    this->uart = NULL;
}

static const io_vtable_t io_vtable = {
    .read = ioread,
    .write = iowrite,
    .seek = ioseek,
    .ctl = ioctl,
    .flush = ioflush,
    .close = ioclose,
};

static io_t* find_free(void)
{
    for (int i = 0; i < IO_UART_NUM; i++) {
        if (!io_uart[i].vtable) {
            return &io_uart[i];
        }
    }
}

io_t* io_uart_init(uart_t* uart)
{
    io_t* this = allocate(sizeof(io_t));
    if (!this)
        return NULL;
    this->vtable = &io_vtable;
    this->uart = uart;
    return this;
}
