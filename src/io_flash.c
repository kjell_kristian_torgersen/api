#include <stdarg.h>
#include <stddef.h>

#include "api/allocate.h"
#include "api/flash.h"
#include "api/io.h"

struct io {
    const io_vtable_t* vtable;
    flash_t* flash;
    uint32_t addr;
};

static int ioread(io_t* this, void* buf, int len)
{
    int n = flash_read(this->flash, this->addr, buf, len);
    if (n > 0)
        this->addr += n;
    return n;
}

static int iowrite(io_t* this, const void* buf, int len)
{
    int n = flash_write(this->flash, this->addr, buf, len);
    if (n > 0)
        this->addr += n;
    return n;
}

static int ioseek(io_t* this, int offset, io_whence_t whence)
{
    switch (whence) {
    case IO_SEEK_SET:
        this->addr = offset;
        return this->addr;
        break;
    case IO_SEEK_CUR:
        this->addr += offset;
        return this->addr;
    case IO_SEEK_END:
        this->addr = flash_size(this->flash) + offset;
        return this->addr;
    default:
        return this->addr;
    }

    return 0;
}

static int ioctl(io_t* this, int request, va_list arg)
{
    return -1;
}

static void ioflush(io_t* this)
{
}

static void ioclose(io_t* this)
{
    flash_close(this->flash);
}

static const io_vtable_t io_vtable = {
    .read = ioread,
    .write = iowrite,
    .seek = ioseek,
    .ctl = ioctl,
    .flush = ioflush,
    .close = ioclose,
};

io_t* io_flash_init(flash_t* flash)
{
    io_t* this = allocate(sizeof(io_t));
    if (!this)
        return NULL;
    this->vtable = &io_vtable;
    this->flash = flash;
    this->addr = 0;
    return this;
}
