#include "api/flash.h"

struct flash {
    const flash_vtable_t* vtable;
};
#define VERIFY(x)                \
    if (!this)                   \
        return RET_NULL_POINTER; \
    if (!this->vtable)           \
        return RET_NULL_POINTER; \
    if (!this->vtable->x)        \
        return RET_NULL_POINTER;

ret_t flash_set_callback(flash_t* this, flash_done_t cb, void* arg)
{
    VERIFY(set_callback);
    return this->vtable->set_callback(this, cb, arg);
}

ret_t flash_read(flash_t* this, uint32_t addr, void* data, unsigned count)
{
    VERIFY(read);
    return this->vtable->read(this, addr, data, count);
}

ret_t flash_write(flash_t* this, uint32_t addr, const void* data, unsigned count)
{
    VERIFY(write);
    return this->vtable->write(this, addr, data, count);
}

ret_t flash_erase(flash_t* this, uint32_t addr, uint32_t count)
{
    VERIFY(erase);
    return this->vtable->erase(this, addr, count);
}

/** @brief perform a command to the flash. Provide a pointer to a corresponding variable of the command
 * 	type and the size of the variable. The command will be performed asynchronously.
 * 	@param this pointer to the flash object
 * 	@param command the command to be performed
 * 	@param arg pointer to the data to be used for the command and return value
 */
ret_t flash_command(flash_t* this, flash_commands_t command, void* arg)
{
    VERIFY(command);
    return this->vtable->command(this, command, arg);
}

ret_t flash_close(flash_t* this)
{
    VERIFY(close);
    return this->vtable->close(this);
}
