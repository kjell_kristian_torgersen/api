#ifndef API_TIMER_H_
#define API_TIMER_H_

#include <stdint.h>

uint32_t timer_unixtime_get(void);
void timer_unixtime_set(uint32_t time);
uint32_t timer_us(void);
uint32_t timer_ms(void);
uint32_t timer_s(void);

#endif /* API_TIMER_H_ */
