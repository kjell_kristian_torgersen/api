# Description of microcontroller independent interface

Several C header files exists for implementing basic C functions to use a microcontroller consistently accross completely different platforms. This so the same drivers and other microcontroller dependent code can be re-used on multiple platforms with little effort. 

## Components

| Name | Description |
| ---- | ----------- |
| allocate | Support somewhat dynamic memory allocation by using a large array of bytes. Doesn't support freeing memory. |
| debug | debug printf function. For sending messages to the developer. |
| delay | Blocking delay. *Should normally not be used except when short and exact delays are required.* |
| flash | For accessing flash in an unified way. |
| gpio | For doing GPIO read/write in an unified way. |
| i2c | For doing I2C read/write in an unified way. |
| io | For doing general IO read/write in an unified way. |
| mcu | Performing operations like enable/disable interrupts, resetting MCU etc. in an unified way. |
| onewire | Using Onewire |
| ret | Return values for the functions, so they are consistent. |
| spi | For using the SPI bus |
| timer | For using the timer |
| uart | For using the UARTs |

In many cases it is possible to implement the required functionality in software if it is not supported by the microcontroller in hardware.
This interface does not specify if function should be blocking or not (except for the delay module). For a system with a real time operating system with multiple threads, blocking functionality can be most convenient. For code running bare metal, most of the functions should be non-blocking.
