#ifndef API_GPIO_H_
#define API_GPIO_H_

#include <stdbool.h>
#include <stdint.h>

#include "api/ret.h"

typedef enum gpio_config {
    GPIO_INPUT, ///< GPIO is input
    GPIO_INPUT_LOW, ///< Input with pull-down
    GPIO_INPUT_HIGH, ///< Input with pull-up
    GPIO_OUTPUT, ///< Output
    GPIO_OPENDRAIN ///< Open-drain output
} gpio_config_t;

#define PIN(x) (1 << x)

uint32_t gpio_get_pinmask(uint8_t port, unsigned pinmask);
ret_t gpio_config(uint8_t port, unsigned pinmask, gpio_config_t config);
ret_t gpio_toggle_pinmask(uint8_t port, unsigned pinmask);
ret_t gpio_set_pinmask(uint8_t port, unsigned pinmask);
ret_t gpio_clear_pinmask(uint8_t port, unsigned pinmask);
ret_t gpio_set_values(uint8_t port, unsigned pinmask, unsigned value);

#endif /* API_GPIO_H_ */
