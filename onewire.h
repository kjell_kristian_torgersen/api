#ifndef B1C98AE7_40E5_47B5_835E_D4DFB7287DBF
#define B1C98AE7_40E5_47B5_835E_D4DFB7287DBF

#include "ret.h"

typedef struct onewire {
    uint8_t port;
    uint32_t pin;
} onewire_t;

typedef void (*onewire_done_t)(onewire_t*, const uint8_t* tx_buf, uint16_t tx_count, const uint8_t* rx_buf, uint16_t rx_count, void* arg);

#define ONEWIRE_SEARCH_FIRST 0xFF
#define ONEWIRE_PRESENCE_ERROR 0xFF
#define ONEWIRE_DATA_ERROR 0xFE
#define ONEWIRE_LAST_DEVICE 0x00

#define ONEWIRE_MATCH_ROM 0x55
#define ONEWIRE_SKIP_ROM 0xCC

#define ONEWIRE_ROMCODE_SIZE 8

typedef enum ow_status {
    OW_OK = 0,
    OW_NO_PRESENCE = -1,
    OW_BUS_LOW = -2,
    OW_CRC_ERROR = -3,
    OW_NULL_POINTER = 4
} onewire_status_t;

onewire_status_t onewire_reset(onewire_t* this);
ret_t onewire_set_callback(onewire_t* this, onewire_done_t cb, void* arg);
ret_t onewire_read(onewire_t* this, void* data, unsigned count);
ret_t onewire_write(onewire_t* this, const void* data, unsigned count);
ret_t onewire_power(onewire_t* this, bool enable);
ret_t onewire_handle_id(onewire_t* this, const uint8_t* id);
uint8_t onewire_search_rom(onewire_t* this, uint8_t diff, uint8_t* id);

#endif /* B1C98AE7_40E5_47B5_835E_D4DFB7287DBF */
