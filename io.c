#include <stdarg.h>

#include "io.h"

#ifdef API_DEBUG
#define VERIFY_VTABLE(z)         \
    if (!this)                   \
        return RET_NULL_POINTER; \
    if (!this->vtable)           \
        return RET_NULL_POINTER; \
    if (!this->vtable->z)        \
        return RET_NULL_POINTER;
#else
#define VERIFY_VTABLE(x)
#endif

struct io {
    const io_vtable_t* vtable;
};

ret_t io_read(io_t* this, void* buf, int len)
{
    VERIFY_VTABLE(read);
    return this->vtable->read(this, buf, len);
}

ret_t io_write(io_t* this, const void* buf, int len)
{
    VERIFY_VTABLE(write);
    return this->vtable->write(this, buf, len);
}

ret_t io_seek(io_t* this, int offset, whence_t whence)
{
    VERIFY_VTABLE(seek);
    return this->vtable->seek(this, offset, whence);
}

ret_t io_ctl(io_t* this, int request, ... /* arg */)
{
    VERIFY_VTABLE(ctl);
    va_list arg;
    va_start(arg, request);
    int ret = this->vtable->ctl(this, request, arg);
    va_end(arg);
    return ret;
}

ret_t io_flush(io_t* this)
{
    VERIFY_VTABLE(flush);
    this->vtable->flush(this);
    return RET_OK;
}

ret_t io_close(io_t* this)
{
    VERIFY_VTABLE(close);
    this->vtable->close(this);
    return RET_OK;
}