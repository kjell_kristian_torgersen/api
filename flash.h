#ifndef A212412F_516D_4F1C_9C8D_A1B783709F01
#define A212412F_516D_4F1C_9C8D_A1B783709F01

#include <stdbool.h>
#include <stdint.h>

#include "api/ret.h"
#include "io.h"

typedef struct flash flash_t;
typedef struct spi spi_t;

typedef void (*flash_done_t)(flash_t*, const uint8_t* tx_buf, uint16_t tx_count, const uint8_t* rx_buf, uint16_t rx_count, void* arg);

typedef enum flash_commands {
    FLASH_GET_SIZE, ///< use together with flash_get_size_t
    FLASH_GET_STATUS, ///< use together with flash_get_status_t
    FLASH_BUSY, ///< use together with flash_busy_t
    FLASH_SET_BLOCKING ///< use together with flash_set_blocking_t
} flash_commands_t;

typedef struct flash_vtable {
    ret_t (*set_callback)(flash_t* this, flash_done_t cb, void* arg);
    ret_t (*read)(flash_t* this, uint32_t addr, void* data, unsigned count);
    ret_t (*write)(flash_t* this, uint32_t addr, const void* data, unsigned count);
    ret_t (*erase)(flash_t* this, uint32_t addr, uint32_t count);
    ret_t (*command)(flash_t* this, flash_commands_t command, void* arg);
    ret_t (*close)(flash_t* this);
} flash_vtable_t;

typedef struct flash_get_size {
    uint32_t page_size; // The smallest erasable unit in bytes
    uint32_t total_size; // The total size of the flash in bytes
} flash_get_size_t;

typedef uint32_t flash_get_status_t;
typedef bool flash_busy_t;
typedef bool flash_set_blocking_t;

ret_t flash_set_callback(flash_t* this, flash_done_t cb, void* arg);
ret_t flash_read(flash_t* this, uint32_t addr, void* data, unsigned count);
ret_t flash_write(flash_t* this, uint32_t addr, const void* data, unsigned count);
ret_t flash_erase(flash_t* this, uint32_t addr, uint32_t count);
/** @brief perform a command to the flash. Provide a pointer to a corresponding variable of the command
 * 	type and the size of the variable.
 * 	@param this pointer to the flash object
 * 	@param command the command to be performed
 * 	@param arg pointer to the data to be used for the command and return value
 */
ret_t flash_command(flash_t* this, flash_commands_t command, void* arg);
ret_t flash_close(flash_t* this);
/** \brief Use the internal flash of the MCU. This function can be called multiple times with different start addresses and sizes to emulate multiple independent flashes.
 * \param start_addr is mapped to address zero when using flash_read(), flash_write() and flash_erase().
 * \param size is used to ensure that the user cannot write outside the area specified by start_addr to start_addr+size-1.
 */
flash_t* flash_mcu_init(uint32_t start_addr, uint32_t size);
/** \brief General purpose SPI flash driver. This function can be called multiple times with different start addresses and sizes to emulate multiple independent flashes. Beware that a chip erase will erase all the "virtual" flashes as well.
 * \param spi pointer to the SPI driver to use
 * \param cs_port port to use for the chip select
 * \param cs_pin pin to use for the chip select
 * \param start_addr is mapped to address zero when using flash_read(), flash_write() and flash_erase().
 * \param size is used to ensure that the user cannot write outside the area specified by start_addr to start_addr+size-1.
 */
flash_t* flash_spi_init(spi_t* spi, uint8_t cs_port, uint8_t cs_pin, uint32_t start_addr, uint32_t size);
#endif /* A212412F_516D_4F1C_9C8D_A1B783709F01 */
