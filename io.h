#ifndef EC0029B3_9F4F_443D_B391_B6E91203EC4B
#define EC0029B3_9F4F_443D_B391_B6E91203EC4B

#include <stdarg.h>
#include <stdint.h>

#include "api/ret.h"

typedef struct flash flash_t;
typedef struct uart uart_t;

typedef struct io io_t;

#ifndef SEEK_CUR
typedef enum whence { SEEK_SET,
    SEEK_CUR,
    SEEK_END } whence_t;
#else
typedef int whence_t;
#endif

typedef struct io_vtable {
    ret_t (*read)(io_t* this, void* buf, int len);
    ret_t (*write)(io_t* this, const void* buf, int len);
    ret_t (*seek)(io_t* this, int offset, whence_t whence);
    ret_t (*ctl)(io_t* this, int request, va_list arg);
    void (*flush)(io_t* this);
    void (*close)(io_t* this);
} io_vtable_t;

ret_t io_read(io_t* this, void* buf, int len);
ret_t io_write(io_t* this, const void* buf, int len);
ret_t io_seek(io_t* this, int offset, whence_t whence);
ret_t io_ctl(io_t* this, int request, ... /* arg */);
ret_t io_flush(io_t* this);
ret_t io_close(io_t* this);

io_t* io_uart_init(uart_t* uart);
io_t* io_flash_init(flash_t* flash);

#endif /* EC0029B3_9F4F_443D_B391_B6E91203EC4B */
