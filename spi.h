#ifndef EF8278E6_1254_4E83_B9D9_9B8F5926CFBB
#define EF8278E6_1254_4E83_B9D9_9B8F5926CFBB

#include <stdint.h>

#include "api/ret.h"

typedef enum spi_mode { SPI_MODE_CPHA = 1,
    SPI_MODE_CPOL = 2 } spi_mode_t;
typedef struct spi spi_t;

typedef void (*spi_txrx_done_cb)(spi_t* spi, const uint8_t* txbuf, int txbufSize, const uint8_t* rxbuf, int rxbufSize, void* arg);

ret_t spi_close(spi_t* this);
ret_t spi_mode(spi_t* this, spi_mode_t mode);
ret_t spi_read(spi_t* this, void* buf, int count);
ret_t spi_readwrite(spi_t* this, void* rxbuf, const void* txbuf, int count);
ret_t spi_set_callback(spi_t* this, spi_txrx_done_cb cb, void* arg);
ret_t spi_write(spi_t* this, const void* buf, int count);

#endif /* EF8278E6_1254_4E83_B9D9_9B8F5926CFBB */
